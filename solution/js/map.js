var view = new ol.View({
    center: [0, 0],
    zoom: 1
});

var map = new ol.Map({
    layers: [new ol.layer.Tile({
        source: new ol.source.OSM()
    })],
    target: 'map',
    view: view
});

var features = [];

function locationSet(param) {
    var Point = ol.geom.Point;
    var c = ol.proj.fromLonLat([param['lon'], param['lat']]);
    p = new Point(c);

    if (!('show' in param)) {
        param['show'] = false;
    }
    if (!('zoom' in param)) {
        param['zoom'] = 9;
    }

    if (param['show']) {
        var marker = new ol.Feature({
            geometry: p,
        });

        var markers = new ol.source.Vector({
            features: [marker]
        });

        var markerVectorLayer = new ol.layer.Vector({
            source: markers,
        });
        map.addLayer(markerVectorLayer);
    }
    map.getView().animate({
        "zoom": param['zoom'],
        "center": c
    });
    myRefresh();
}

function reset() {
    layers = map.getLayers().getArray();
    if (layers.length < 2)
        return;
    layer = layers[1];
    /*
    features = layer.getSource().getFeatures();
    for (i in features.length) {
      layer.removeFeatures(features[i]);
    }
    */
    map.removeLayer(layer);
    node = document.getElementById("location")
    while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
    }
}

function myRefresh() {
    console.log("myRefresh called");
}