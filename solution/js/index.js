  new Vue({
      el: '#app',
      data: {
          airports: [],
          airportsMatching: [],
          distanceMax: 10,
          lat: 46.234120,
          lon: 6.052646,
          status: ""
      },
      mounted() {
          self = this;
          fetch('api/airports')
              .then(function(response) {
                  return response.json();
              })
              .then(function(myJson) {
                  console.log("api/airports request -> DONE");
                  self.airports = myJson['rows'];
              });

          fetch('api/geodecode/ip')
              .then(function(response) {
                  return response.json();
              })
              .then(function(myJson) {
                  console.log("api/geodecode/ip request -> DONE");
                  locationSet(myJson);
              });


          document.getElementById("searchText").focus();
      },
      methods: {
          compute: function(event) {
              self = this;
              self.airportsMatching = [];

              for (i = 0; i < self.airports.length; i++) {
                  distance = getDistanceFromLatLonInKm(self.airports[i].lat, self.airports[i].lon,
                      self.lat, self.lon);
                  if (distance <= self.distanceMax) {
                      distance = Math.round(distance * 10) / 10;
                      self.airportsMatching.push(self.airports[i].name + " (distance: " + distance + " km)");
                  }
              }


              if (self.airportsMatching.length > 0) {
                  self.status = "Liste des aéroports à moins de " + self.distanceMax + " km de distance";
              } else {
                  self.status = "Aucun aéroport à cette distance.";
              }
          }
      }
  });