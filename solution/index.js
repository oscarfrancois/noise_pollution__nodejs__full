/* Main project file. */

const express = require('express');
const fs = require('fs');
const open = require('open');
const portfinder = require('portfinder');
const syncRequest = require('sync-request');

// Instanciate the web server.
let app = express();

/* Get the list of airports in the world in json format.
 * @returns {Object} a json object containing the list of airports.
 */
app.get('/api/airports', function(req, res) {
    let TAG = 'airports';
    if (!fs.existsSync('cache/' + TAG + '.json'))
        cacheCreate('cache/' + TAG + '.json');
    fs.readFile('cache/' + TAG + '.json', (err, data) => {
        if (err) throw err;
        res.json(JSON.parse(data));
    });
});

/* Resolve an ip into a lat,long in json format.
 * @returns {Object} {'lon': {number}, 'lat': {number}}
 */
app.get('/api/geodecode/ip', function(req, res) {
    let TAG = 'geodecodeIp';
    let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var regIp = /\d+\.\d+\.\d+\.\d+/i;
    var result = ip.match(regIp);
    console.log(TAG + ' ' + result);
    if (result == null) {
        console.log(TAG + ' seems a local ip call -> lookup on ifconfig.me website');
        let res = syncRequest('GET', 'https://ifconfig.me');
        ip = res.getBody('utf8');
        console.log(TAG + ' ip lookedup = ' + ip);
    }
    let url = 'http://ip-api.com/json/' + ip;
    console.log(TAG + ' ip to geodecode ->' + ip);
    let resIp = syncRequest('GET', url);
    data = resIp.getBody('utf8');
    data = JSON.parse(data);
    data = {
        'lon': data['lon'],
        'lat': data['lat']
    };
    res.json(data);
});

/* Serves static content. */
app.use(express.static(__dirname));

/* Creates a cache file for the airports in json format. */
function cacheCreate(filename) {
    let TAG = 'cacheCreate ';
    console.log(TAG + 'start');
    let url = 'https://www.flightradar24.com/_json/airports.php';
    let res = syncRequest('GET', url);
    let data = res.getBody('utf8');
    fs.writeFileSync(filename, data, (err) => {
        if (err) throw err;
        console.log(TAG + 'the file has been saved');
    });
    console.log(TAG + 'end');
}

/* Listen on the first port available 
 * then browse this url.
 */
portfinder.getPortPromise()
  .then((portFound) => {
    let server = app.listen(portFound, function () {
      let host = server.address().address;
      port = server.address().port;
      console.log("app listening at: http://%s:%s", host, port);
      open("http://localhost:" + port);
    })
  })
  .catch((err) => {
    console.log("can't find an available port within: [" + portfinder.basePort + ';' + portfinder.highestPort + ']');
  });
