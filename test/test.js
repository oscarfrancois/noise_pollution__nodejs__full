/*
Project: Noise pollution
Description: Partially automated tests for this project


Notions pré-requises pour la création de tests automatisés:

- Accéder à un element de la page HTML: 
    - document.getElementById("MON_ID");
    - document.getElementsByTagName("MON_NOM");

- Cliquer sur un élément:
    let elmnt = document.getElementById("MON_ID");
    elmnt.click();

- Accéder à l'ensemble du texte affiché dans la page courante: document.body.innerText

- Accéder au texte associé à une balise de type bouton:
  let btn = document.getElementsByTagName("button")[0];
  btn.textContent;

- Notion d'expression régulière et création d'une règle pour vérifier la conformité d'une réponse:

Différents mécanismes sont à disposition.
Nous faisons le choix ici de présenter seulement l'utilisation d'expression régulière avec la méthode search d'une chaine de caractères.

let str = "w3school est tip-top";
var n = str.search("W3Schools/i");
if (n !=  -1) {
  console.log("chaîne trouvée");
}
(cf. https://www.w3schools.com/js/js_regexp.asp)

*/

/**
 * Dedicated test for CERN position.
 * Just to show a test based on a regexp to validate the result 
**/
function testCern10km() {
  console.log("testing cern");
  // TODO: uncomment when fillAndValidate is ready: let resExpectedReg = /geneva.*international.*airport.*4.4.*km/i;
  let cernPos = {'lat': 46.234120, 'lng': 6.052646};
}

/**
 * Launch the tests for all the positions required in the specifications
 * cf. https://gitlab.com/oscarfrancois/nodejs__javascript__noise_pollution/
**/
function test() {
  console.log("test called");
  let conditions = [
      {"name": "cern", "lat": 46.234120, "lng": 6.052646, "rangeMax": 10},
      {"name": "zurich", "lat": 47.448030, "lng": 8.583181, "rangeMax": 10},
      {"name": "jardins du luxembourg, Paris", "lat": 48.847554, "lng": 2.336390, "rangeMax": 15}
    ];
  for (let i=0; i<conditions.length; i++) {
    setTimeout(function() { console.log('checking ' + conditions[i]["name"]); fillAndValidate(conditions[i])}, 5000*i);
  }
}

/**
 * Click on the button tag at a specified index
 * @param {number} index - the position of the button to click on (first position == 0)
**/
function clickButton(index) {
  console.log("clickButton called.");
  let buttons = document.getElementsByTagName("button");
  buttons[index].click();
}


/**
 * Click on the button tag matching a specific text
 * @param {string} text - the position of the button to click on (first position == 0)
**/
function clickButtonReg(text) {
  console.log("clickButton called");
  let reg = new RegExp(".*" + text + ".*");
  let buttons = document.getElementsByTagName("button");
  for (let i=0; i<buttons.length; i++) {
    let button = buttons[i];
    if (reg.test(button.textContent)) {
      console.log(reg.toString() + " found at index: " + i + ". textContent=" + button.textContent);
      button.click();
    }
  }
}

/**
 * Fill the form and click on the button to display the airports matching the criteria
 * @param {object} pos - json object containing the mandatory properties: lat, lng
**/
function fillAndValidate(pos) {
  console.log("pos['lat']=" + pos['lat'] + ", pos['lng']=" + pos['lng']);
  // TODO: set the tag ids in the requirements to make the test more reliable
  document.getElementsByTagName("input")[0].value = pos['lat'];
  document.getElementsByTagName("input")[1].value = pos['lng'];
  document.getElementsByTagName("input")[2].value = pos['rangeMax'];
  clickButton(0);
  //TODO: add a regexp.test to verify the expected output
}

