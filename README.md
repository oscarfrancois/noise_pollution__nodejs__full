# Cartographie des nuisances sonores aériennes

![beluga](img/beluga_320_180.jpeg)


## Mise en contexte

La gêne sonore est un sujet d'environnement mieux reconnu et étudié de nos
jours.
Il existe de nombreuses sources de bruit (bruit ambient dans une pièce,
voisinage, circulation routière, avions, événement, festival, etc).

Nous traiterons dans cette activité de la gêne sonore induite par les avions.

A titre d'illustration, voici le traffic mondial sur une journée.

![timelapse](video/world_airline__traffic__24-hour__timelapse.webm)


## Besoin client

Un client vous charge de réaliser une application web permettant de connaître
les nuisances sonores aériennes à proximité d'un lieu.

Comme première étape il vous demande de créer une interface où
l'on peut saisir une coordonnée GPS (latitude et longitude) et que l'application
indique si un aéroport se trouve à proximité.

A ce stade du développement, seul l'aspect fonctionnel compte:

![calcul des aéroports dans une zone](img/distance_computation_default.png)

Figure 1: Vue par défaut à afficher

![calcul des aéroports dans une zone](img/distance_computation_example.png)

Figure 2: Exemple d'utilisation

La distance maximale doit avoir pour valeur par défaut 10km. Elle doit être paramétrable.


## Architecture technique

Votre responsable technique a fait les choix techniques suivants que vous devez
respecter:

- Le back-end sera réalisé au choix en Javascript via le cadre applicatif
("framework") nodejs ou bien en PHP.

- Le front-end doit utiliser le framework [vuejs](https://vuejs.org)


## Données techniques à disposition

API retournant les principaux aéroports dans le monde au format json (avec leurs coordonnées GPS):

 https://www.flightradar24.com/_json/airports.php

Le calcul des distances devra être effectué dynamiquement en javascript
directement dans le navigateur web via la formule [Haversine](js/latlon.js).


## Jeu de tests:

Le jeu de tests suivant permettra de valider le bon fonctionnement de votre
application web.

  Si l'utilisateur renseigne les coordonnées du CERN: 46.234120, 6.052646,
  l'application doit afficher l'aéroport de:

  - Geneva International Airport (distance: 4.4 km) 

  Si l'utilisateur renseigne les coordonnées de l'arrêt de train de Kloten:
  47.448030, 8.583181,
  l'application doit afficher l'aéroport de:

  - Zurich Airport (distance: 3.2 km) 

  Si l'utilisateur renseigne les coordonnées du Jardin du Luxembourg à Paris:
  48.847554, 2.336390 et une distance de 15km,
  l'application doit afficher les aéroports de:

  - Paris Le Bourget Airport (distance: 14.3 km)
  - Paris Orly Airport (distance: 14.2 km)

L'application doit aussi vérifier que les champs sont correctement saisis et sinon
afficher un message d'erreur adapté.
